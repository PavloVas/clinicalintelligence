﻿using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ClinicalIntelligence.Helpers
{
    public class ConnectivityHelper
    {
        public static async Task<bool> HasConnection()
        {
            return CrossConnectivity.Current.IsConnected && await CrossConnectivity.Current.IsRemoteReachable("google.com");
        }
    }
}