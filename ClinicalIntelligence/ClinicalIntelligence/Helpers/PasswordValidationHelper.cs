﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace ClinicalIntelligence.Helpers
{
    public static class PasswordValidationHelper
    {
        const string passwordRegex = @"(^(?=.*[A-Z]).{5,15}$)";

        public static bool PasswordValidation(string email)
        {
            return !Regex.IsMatch(email, passwordRegex);
        }
    }
}
