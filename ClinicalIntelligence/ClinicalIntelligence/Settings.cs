﻿using Newtonsoft.Json;
using Plugin.Settings;
using Plugin.Settings.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace ClinicalIntelligence
{
    public static class Settings
    {
        private static ISettings _appSettings;
        public static ISettings AppSettings
        {
            get
            {
                if (Device.OS == TargetPlatform.Windows && _appSettings != null)
                {
                    return _appSettings;
                }
                return CrossSettings.Current; ;
            }
            set
            {
                _appSettings = value;
            }
        }

        //public static bool IsUserSignedIn
        //{
        //    get
        //    {
        //        return !string.IsNullOrWhiteSpace(AuthKey) &&
        //                      !string.IsNullOrWhiteSpace(UserId) &&
        //                      !string.IsNullOrWhiteSpace(FirmId) &&
        //                      FirmModel != null;
        //    }
        //}

        //public static long LastSyncDate { get { return Get<long>("LastSyncDate", 0); } set { Set("LastSyncDate", value); } }
        //public static string AuthKey { get { return Get<string>("AuthKey"); } set { Set("AuthKey", value); } }
        //public static string FirmId { get { return Get<string>("FirmId"); } set { Set("FirmId", value); } }
        //public static string UserId { get { return Get<string>("UserId"); } set { Set("UserId", value); } }
        public static string UserEmail { get { return Get<string>("UserEmail"); } set { Set("UserEmail", value); } }
        public static string Token { get { return Get<string>("Token"); } set { Set("Token", value); } }
        public static string PatientId { get { return Get<string>("PatientId"); } set { Set("PatientId", value); } }

        
        //public static int UserFirmsCount { get { return Get<int>("UserFirmsCount"); } set { Set("UserFirmsCount", value); } }
        //public static FirmModel FirmModel { get { return Get<FirmModel>("FirmModel"); } set { Set("FirmModel", value); } }

        //public static string ServerDateTime { get { return Get<string>("ServerDateTime"); } set { Set("ServerDateTime", value); } }

        //public static bool AskBeforeDelete { get { return Get<bool>("AskBeforeDelete", true); } set { Set("AskBeforeDelete", value); } }

        //public static string AppName { get { return Get<string>("AppName"); } set { Set("AppName", value); } }
        //public static bool IsBasic { get { return !IsBilling; } }
        //public static bool IsBilling { get { return Get<bool>("IsBilling"); } set { Set("IsBilling", value); } }
        //public static bool IsBillingAndLegal { get { return Get<bool>("IsBillingAndLegal"); } set { Set("IsBillingAndLegal", value); } }
        //public static bool IsAdmin { get { return Get<bool>("IsAdmin"); } set { Set("IsAdmin", value); } }

        //public static int FirmTimerRoundingHours { get { return Get<int>("TimerRoundingHours", 0); } set { Set("TimerRoundingHours", value); } }
        //public static int FirmTimerRoundingMinutes { get { return Get<int>("TimerRoundingMinutes", 1); } set { Set("TimerRoundingMinutes", value); } }

        //public static bool HasActiveTimers { get { return Get<bool>("HasActiveTimers", false); } set { Set("HasActiveTimers", value); } }
        //public static int TimersCount { get { return Get<int>("TimersCount", 0); } set { Set("TimersCount", value); } }

        //public static string Language { get { return Get<string>("Language", ""); } set { Set("Language", value); } }
        //public static string CurrencyCode { get { return Get<string>("CurrencyCode", "USD"); } set { Set("CurrencyCode", value); } }

        //public static bool IsAlreadyInGoogleAnalytics { get { return Get<bool>(nameof(IsAlreadyInGoogleAnalytics), false); } set { Set(nameof(IsAlreadyInGoogleAnalytics), value); } }

        //public static DateTime LastDateShowRating { get { return Get<DateTime>(nameof(LastDateShowRating), DateTime.MinValue); } set { Set(nameof(LastDateShowRating), value); } }
        //public static bool IsUserAlreadyLeaveReview { get { return Get<bool>(nameof(IsUserAlreadyLeaveReview), false); } set { Set(nameof(IsUserAlreadyLeaveReview), value); } }

        //public static bool IsInitialSyncCompleted { get { return Get<bool>(nameof(IsInitialSyncCompleted), false); } set { Set(nameof(IsInitialSyncCompleted), value); } }

        //public static LocationPermissionPopupValue LocationPermissionPopupValue { get { return Get<LocationPermissionPopupValue>(nameof(LocationPermissionPopupValue), LocationPermissionPopupValue.NotAsked); } set { Set(nameof(LocationPermissionPopupValue), value); } }

        //public static Position LastPosition { get { return Get<Position>(nameof(LastPosition), null); } set { Set(nameof(LastPosition), value); } }

        public static void ClearLoginData()
        {
            //AuthKey = null;
            //FirmId = null;
            //UserId = null;
            ////UserEmail = null;
            //UserFirmsCount = 0;
            //FirmModel = null;
            //FirmTimerRoundingHours = 0;
            //FirmTimerRoundingMinutes = 1;

            //AskBeforeDelete = true;

            //IsBilling = false;
            //IsBillingAndLegal = false;
            //IsAdmin = false;

            //HasActiveTimers = false;
            //TimersCount = 0;

            //LastSyncDate = 0;

            //LocationPermissionPopupValue = LocationPermissionPopupValue.NotAsked;
        }

        private static Dictionary<string, object> _cache = new Dictionary<string, object>();

        private static T Get<T>(string name, T defaultValue = default(T))
        {
            try
            {
                if (_cache.ContainsKey(name) && _cache[name] != null)
                {
                    return (T)_cache[name];
                }

                var jsonContent = AppSettings.GetValueOrDefault(name, "");
                if (string.IsNullOrWhiteSpace(jsonContent))
                {
                    return defaultValue;
                }

                var result = JsonConvert.DeserializeObject<T>(jsonContent);

                _cache[name] = result;

                return result;
            }
            catch
            {
                return defaultValue;
            }
        }

        private static void Set<T>(string name, T value)
        {
            var jsonContent = JsonConvert.SerializeObject(value);

            AppSettings.AddOrUpdateValue(name, jsonContent);

            if (_cache.ContainsKey(name))
            {
                _cache[name] = null;
            }
        }
    }
}