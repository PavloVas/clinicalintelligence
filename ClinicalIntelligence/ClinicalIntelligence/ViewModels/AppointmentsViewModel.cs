﻿using ClinicalIntelligence.Controls;
using ClinicalIntelligence.Models;
using ClinicalIntelligence.Popups;
using ClinicalIntelligence.Views;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using XamForms.Controls;

namespace ClinicalIntelligence.ViewModels
{
    public class AppointmentsViewModel : BaseViewModel<ContentPage>
    {

        private ICommand _clickAttendanceCommand;
        public ICommand ClickAttendanceCommand
        {
            get { return _clickAttendanceCommand; }
        }

        private ObservableCollection<SpecialDate> _attendancesCalendar;
        public ObservableCollection<SpecialDate> AttendancesCalendar
        {
            get { return _attendancesCalendar; }
            set
            {
                _attendancesCalendar = value;
                OnPropertyChanged();
            }
        }

        private int _checkBoxSelect;
        public int CheckBoxSelect
        {
            get { return _checkBoxSelect; }
            set
            {
                _checkBoxSelect = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<AppointmentsModels> _attendances;
        public ObservableCollection<AppointmentsModels> Attendances
        {
            get { return _attendances; }
            set
            {
                _attendances = value;
                OnPropertyChanged();
            }
        }


        private QuestionnaireModel _attendancesSelectItem;
        public QuestionnaireModel AttendancesSelectItem
        {
            get { return _attendancesSelectItem; }
            set
            {
                _attendancesSelectItem = value;
                OnPropertyChanged();
            }
        }
        
        private DateTime? _date;
        public DateTime? Date
        {
            get { return _date; }
            set
            {
                _date = value;
                if (_date != null)
                {
                    TempDate = _date.Value;
                }
                OnPropertyChanged();
            }
        }

        private DateTime _tempDate;
        public DateTime TempDate
        {
            get { return _tempDate; }
            set
            {
                _tempDate = value;               
                OnPropertyChanged();
            }
        }



        public ICommand RightArrowCommand { private set; get; }
        public ICommand LeftArrowCommand { private set; get; }
        

        public AppointmentsViewModel(AppointmentsView view) : base(view)
        {

           Date = DateTime.Now;

            LeftArrowCommand = new Command(async (obj) =>
            {
                Device.StartTimer(new TimeSpan(0,0,1), () =>
                {
                    Date = TempDate.AddMonths(-1); return false;
                });                
            });

            RightArrowCommand = new Command(async (obj) =>
            {
                Device.StartTimer(new TimeSpan(0, 0, 1), () =>
                {
                    Date = TempDate.AddMonths(1); return false;
                });
            });

            _clickAttendanceCommand = new Command(async (obj) =>
            {
                if (obj is AppointmentsData)
                {
                    Date = TempDate.AddMonths(1);
                    var popup = new AppointmentsPopup((AppointmentsData)obj);
                    await PopupNavigation.PushAsync(popup);

                   popup.MessageEvent+= (s, e) =>
                   {
                       if (s is AppointmentsData)
                       {
                         
                       }
                   };

                    popup.CallEvent += (s, e) =>
                    {
                        if (s is AppointmentsData)
                        {

                        }
                    };

                    popup.CamEvent += (s, e) =>
                    {
                        if (s is AppointmentsData)
                        {

                        }
                    };

                    popup.ChatEvent += (s, e) =>
                    {
                        if (s is AppointmentsData)
                        {

                        }
                    };

                }
            });


            AttendancesCalendar = new ObservableCollection<SpecialDate>();

            Attendances = new ObservableCollection<AppointmentsModels>();


            //AttendancesCalendar.Add(new SpecialDate(DateTime.Now)
            //{
            //    Selectable =true, BackgroundPattern = new BackgroundPattern(1) { Pattern = new List<Pattern> { new Pattern { Text = "3", TextColor = Color.Red, TextSize = 9 } } }
            //});

            CheckBoxSelect = (int)AppointmentsSort.Past;

            var eee = new AppointmentsModels() { new AppointmentsData() {Image= "ic_stethoscope", Doctor = "Doctor", ReceptionTime = "60.40pm-07:30", Title = "Physical Appointment" } };
            eee.GroupName = "Today";

            Attendances.Add(eee);
            


            var eee1 = new AppointmentsModels() { new AppointmentsData() { Image = "ic_stethoscope", Doctor = "Doctor", ReceptionTime = "60.40pm-07:30", Title = "Physical Appointment" } };
            eee1.GroupName = "Future";

            Attendances.Add(eee1);
            


        }
    }
}
