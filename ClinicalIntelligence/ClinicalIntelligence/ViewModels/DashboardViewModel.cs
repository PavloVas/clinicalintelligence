﻿using ClinicalIntelligence.Models;
using ClinicalIntelligence.Popups;
using ClinicalIntelligence.Views;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace ClinicalIntelligence.ViewModels
{
    public class DashboardViewModel : BaseViewModel<ContentPage>
    {
        public ICommand MyDoctorsCommand { private set; get; }
        public ICommand AppointmentsCommand { private set; get; }
        public ICommand QuestionnaireCommand { private set; get; }

        private ICommand _clickNotificationCommand;
        public ICommand ClickNotificationCommand
        {
            get { return _clickNotificationCommand; }
        }

        private ObservableCollection<NotificationsModels> _notifications;
        public ObservableCollection<NotificationsModels> Notifications
        {
            get { return _notifications; }
            set
            {
                _notifications = value;              
                OnPropertyChanged();
            }
        }


        private int _notificationsCount;
        public int NotificationsCount
        {
            get { return _notificationsCount; }
            set
            {
                _notificationsCount = value;
                OnPropertyChanged();
            }
        }

        private int _pendingCount;
        public int PendingCount
        {
            get { return _pendingCount; }
            set
            {
                _pendingCount = value;
                OnPropertyChanged();
            }
        }


        private NotificationsModels _notificationsSelectItem;
        public NotificationsModels NotificationsSelectItem
        {
            get { return _notificationsSelectItem; }
            set
            {
                _notificationsSelectItem = value;
                //if (value != null)
                   // MessagingCenter.Send(this, "HomePage", value.Id);
                OnPropertyChanged();
            }
        }


        string name;

        public DashboardViewModel(DashboardView view) : base(view)
        {
            NotificationsCount = 9;
            PendingCount = 2;

            _clickNotificationCommand = new Command(async (obj) =>
            {
                if (obj is NotificationsModels)
                {
                    var popup = new NotificationPopup((NotificationsModels)obj);
                    await PopupNavigation.PushAsync(popup);

                    popup.AcceptEvent += (s, e) => 
                    {
                        if (s is NotificationsModels)
                        {

                        }
                    };

                    popup.RejectEvent += (s, e) =>
                    {
                        if (s is NotificationsModels)
                        {

                        }
                    };
                }
            });

            MyDoctorsCommand = new Command(async () =>
            {
            });
            AppointmentsCommand = new Command(async () =>
            {
                if (App.Current.MainPage is MasterDetailPage)
                {
                    (App.Current.MainPage as MasterDetailPage).Detail = new NavigationPage(new AppointmentsView()) { BarBackgroundColor = Theme.COLOR_BROWN_BASE, BarTextColor = Color.White };
                }
            });
            QuestionnaireCommand = new Command(async () =>
            {
                if (App.Current.MainPage is MasterDetailPage)
                {
                    (App.Current.MainPage as MasterDetailPage).Detail = new NavigationPage(new QuestionnaireView()) { BarBackgroundColor = Theme.COLOR_BROWN_BASE, BarTextColor = Color.White };
                }
            });

            Notifications = new ObservableCollection<NotificationsModels>();
            int iterator = 0;
            for (int i = 0; i < 5; i++)
            {

                iterator++;

                if (iterator > 4)
                {
                    iterator = 0;
                }

                switch (iterator)
                {
                    case 0:
                        name = "Dr.Jones";
                        break;
                    case 1:
                        name = "Dr.Steve";
                        break;
                    case 2:
                        name = "Dr.Aaron";
                        break;
                    case 3:
                        name = "Dr.Adam";
                        break;
                    case 4:
                        name = "Dr.Albert";
                        break;
                }

                Notifications.Add(new NotificationsModels() {Id=i, Name = name, Qualification = "Profession", Image = "photo.png" });
            }   
            

        }
    }
}
