﻿using ClinicalIntelligence.Models;
using ClinicalIntelligence.Popups;
using ClinicalIntelligence.Views;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace ClinicalIntelligence.ViewModels
{
    public  class QuestionnaireViewModel : BaseViewModel<ContentPage>
    {
        public ICommand NewCommand { private set; get; }
        public ICommand PendingCommand { private set; get; }


        private ICommand _clickQuestionnairesCommand;
        public ICommand ClickQuestionnairesCommand
        {
            get { return _clickQuestionnairesCommand; }
        }

        private ObservableCollection<QuestionnaireModel> _questionnaireS;
        public ObservableCollection<QuestionnaireModel> Questionnaires
        {
            get
            {
                return _questionnaireS;
            }
            set
            {
                _questionnaireS = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<QuestionnaireModel> _questionnairesSorte;
        public ObservableCollection<QuestionnaireModel> QuestionnairesSorte
        {
            get
            {
                return _questionnairesSorte;
            }
            set
            {
                _questionnairesSorte = value;
                OnPropertyChanged();
            }
        }

        private QuestionnaireModel _questionnaireSelectItem;
        public QuestionnaireModel QuestionnaireSelectItem
        {
            get { return _questionnaireSelectItem; }
            set
            {
                _questionnaireSelectItem = value;
                OnPropertyChanged();
            }
        }

        private string _sorteTypeText;
        public string SorteTypeText
        {
            get { return _sorteTypeText; }
            set
            {
                _sorteTypeText = value;
               
                OnPropertyChanged();
            }
        }



        private bool _isSelectInPanelNew;
        public bool IsSelectInPanelNew
        {
            get
            {
                return _isSelectInPanelNew;
            }
            set
            {
                _isSelectInPanelNew = value;
                OnPropertyChanged();
            }
        }


        private void Sort(QuestionnairesStatus questionnairesStatus)
        {
            QuestionnairesSorte = new ObservableCollection<QuestionnaireModel>(Questionnaires.Where(x => x.Status == questionnairesStatus));
            SorteTypeText = Enum.GetName(typeof(QuestionnairesStatus), questionnairesStatus);

            IsSelectInPanelNew = (questionnairesStatus == QuestionnairesStatus.NEW)? true:false;

        }

        public QuestionnaireViewModel(QuestionnaireView view) : base(view)
        {          
            Questionnaires = new ObservableCollection<QuestionnaireModel>();

            Questionnaires.Add(new QuestionnaireModel() { Id = 1, ClinicName = "ClinicName", HealthSurvey = "Patient Health Survey", Status = QuestionnairesStatus.NEW });
            Questionnaires.Add(new QuestionnaireModel() { Id = 1, ClinicName = "ClinicName" , HealthSurvey = "Patient Health Survey" , Status= QuestionnairesStatus.NEW });
            Questionnaires.Add(new QuestionnaireModel() { Id = 1, ClinicName = "ClinicName" , HealthSurvey = "Patient Health Survey" , Status= QuestionnairesStatus.NEW });

            Questionnaires.Add(new QuestionnaireModel() { Id = 3, ClinicName = "ClinicName", HealthSurvey = "Patient Health Survey", Status = QuestionnairesStatus.PENDING});
            Questionnaires.Add(new QuestionnaireModel() { Id = 3, ClinicName = "ClinicName", HealthSurvey = "Patient Health Survey", Status = QuestionnairesStatus.PENDING });

            Questionnaires.Add(new QuestionnaireModel() { Id = 5, ClinicName = "ClinicName", HealthSurvey = "Patient Health Survey", Status = QuestionnairesStatus.FILLED});
            Questionnaires.Add(new QuestionnaireModel() { Id = 5, ClinicName = "ClinicName", HealthSurvey = "Patient Health Survey", Status = QuestionnairesStatus.FILLED });

            Sort(QuestionnairesStatus.NEW);
            
             NewCommand = new Command(async x =>
             {
                 Sort(QuestionnairesStatus.NEW);
             });

            PendingCommand = new Command(async x =>
            {
                Sort(QuestionnairesStatus.PENDING);
            });



            _clickQuestionnairesCommand = new Command(async (obj) =>
            {
                if (obj is QuestionnaireModel)
                {
                    var popup = new QuestionnairePopup((QuestionnaireModel)obj);
                    await PopupNavigation.PushAsync(popup);

                    popup.FilledEvent += async (s, e) =>
                    {
                        if (s is QuestionnaireModel)
                        {
                          
                        }
                    };

                    popup.ViewEvent += async (s, e) =>
                    {
                        if (s is QuestionnaireModel)
                        {
                            
                        }
                    };

                }
            });
        }
    }
}
