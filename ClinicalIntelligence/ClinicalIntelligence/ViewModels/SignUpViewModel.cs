﻿using ClinicalIntelligence.API;
using ClinicalIntelligence.Helpers;
using ClinicalIntelligence.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using XamCountryPicker.Models;

namespace ClinicalIntelligence.ViewModels
{
    public class SignUpViewModel : BaseViewModel<ContentPage>
    {
        public ICommand RegisterNowCommand { private set; get; }

        public ICommand CountryCommand { private set; get; }
        

        private string _infoMessage;
        public string InfoMessage
        {
            get { return _infoMessage; }
            set
            {
                _infoMessage = value;
                OnPropertyChanged();
            }
        }

        private string _firstName;
        public string FirstName
        {
            get { return _firstName; }
            set
            {
                _firstName = value;
                FirstNameAttention = false;
                InfoMessage = string.Empty;
                OnPropertyChanged();
            }
        }

        private bool _firstNameAttention;
        public bool FirstNameAttention
        {
            get { return _firstNameAttention; }
            set
            {
                _firstNameAttention = value;
                OnPropertyChanged();
            }
        }

        private string _lastName;
        public string LastName
        {
            get { return _lastName; }
            set
            {
                _lastName = value;
                LastNameAttention = false;
                InfoMessage = string.Empty;
                OnPropertyChanged();
            }
        }

        private bool _lastNameAttention;
        public bool LastNameAttention
        {
            get { return _lastNameAttention; }
            set
            {
                _lastNameAttention = value;
                OnPropertyChanged();
            }
        }

        private string _email;
        public string Email
        {
            get { return _email; }
            set
            {
                _email = value;
                EmailAttention = false;
                InfoMessage = string.Empty;
                OnPropertyChanged();
            }
        }

        private bool _emailAttention;
        public bool EmailAttention
        {
            get { return _emailAttention; }
            set
            {
                _emailAttention = value;
                OnPropertyChanged();
            }
        }

        private string _username;
        public string Username
        {
            get { return _username; }
            set
            {
                _username = value;
                UsernameAttention = false;
                InfoMessage = string.Empty;
                OnPropertyChanged();
            }
        }

        private bool _usernameAttention;
        public bool UsernameAttention
        {
            get { return _usernameAttention; }
            set
            {
                _usernameAttention = value;
                OnPropertyChanged();
            }
        }        


        private string _password;
        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                PasswordAttention = false;
                ReTypePasswordAttention = false;
                InfoMessage = string.Empty;
               // IsInvalidPassword = false;
                OnPropertyChanged();
            }
        }

        private bool _passwordAttention;
        public bool PasswordAttention
        {
            get { return _passwordAttention; }
            set
            {
                _passwordAttention = value;
                OnPropertyChanged();
            }
        }

        private string _reTypePassword;
        public string ReTypePassword
        {
            get { return _reTypePassword; }
            set
            {
                _reTypePassword = value;
                PasswordAttention = false;
                ReTypePasswordAttention = false;
                InfoMessage = string.Empty;
                OnPropertyChanged();
            }
        }

        private bool _reTypePasswordAttention;
        public bool ReTypePasswordAttention
        {
            get { return _reTypePasswordAttention; }
            set
            {
                _reTypePasswordAttention = value;
                OnPropertyChanged();
            }
        }
            
        private int _gender;
        public int Gender
        {
            get { return _gender; }
            set
            {
                _gender = value;
                OnPropertyChanged();
            }
        }

        private Country _country;
        public Country Country
        {
            get { return _country; }
            set
            {
                _country = value;
                OnPropertyChanged();
            }
        }



        public SignUpViewModel(SignUpView view) : base(view)
        {
            CountryCommand = new Command(async (obj) =>
            {
                Country = (Country)obj;
            });

                RegisterNowCommand = new Command(async () =>
            {

                if (CheckValuesInEntry())
                {
                    return;
                }

                await CheckConnectionAsync();
                if (!HasInternetConnection)
                {
                    return;
                }

                ShowBusy();
                var generatedUsername = await new ApiClient().PostGenerateUsernameAsync(new SignUpArgs() { Email = Email, FirstName = FirstName, LastName = LastName, Password = Password, Username = Username, PatientID = "0" });

                if (!String.IsNullOrEmpty(generatedUsername))
                {
                    var signUp = await new ApiClient().SignUpAsync(new SignUpArgs() { Email = Email, FirstName = FirstName, LastName = LastName, Password = Password, Username = generatedUsername, PatientID = "0" });

                    if (signUp.Success)
                    {
                        App.Current.MainPage = new MasterView();
                    }
                    else
                    {
                        ShowMessagePopupAsync(signUp.MessageError);
                    }
                }
                HideBusy();
            });
        }


        private List<string> elementMessage;
        private bool IsValidationPropertiesProblem;
        private bool CheckValuesInEntry()
        {
            InfoMessage = "";
            IsValidationPropertiesProblem = false;
            elementMessage = new List<string>();


            if (string.IsNullOrEmpty(FirstName))
            {
                FirstNameAttention = true;
                elementMessage.Add("First Name");
            }

            if (string.IsNullOrEmpty(LastName))
            {
                LastNameAttention = true;
                elementMessage.Add("Last Name");
            }

            if (string.IsNullOrEmpty(Email))
            {
                EmailAttention = true;
                elementMessage.Add("Email");
            }          

            if (string.IsNullOrEmpty(Password))
            {
                PasswordAttention = true;
                elementMessage.Add("Password");
            }

            if (string.IsNullOrEmpty(Username))
            {
                UsernameAttention = true;
                elementMessage.Add("Username");
            }

            if ( string.IsNullOrEmpty(ReTypePassword))
            {
                ReTypePasswordAttention = true;
                elementMessage.Add("Re-Type Password");
            }

            if (Gender == 0)
            {
                ReTypePasswordAttention = true;
                elementMessage.Add("Select Prefix");
            }

            if (Country == null)
            {
                ReTypePasswordAttention = true;
                elementMessage.Add("Select Country");
            }

            if (elementMessage.Count > 0)
            {
                foreach (var item in elementMessage)
                {
                    InfoMessage = string.IsNullOrEmpty(InfoMessage) ? item : $"{InfoMessage}, {item}";
                }
                InfoMessage = $"{InfoMessage} {(elementMessage.Count == 1 ? $" {"is"} " : $" {"are"} ")} {"required."} ";
                IsValidationPropertiesProblem = true;
            }


            if (!string.IsNullOrEmpty(Email) && EmailValidationHelper.EmailValidation(Email))
            {
                EmailAttention = true;
                InfoMessage = $"{InfoMessage} {"Email format is not correct."} ";

                IsValidationPropertiesProblem = true;
            }

            if (!string.IsNullOrWhiteSpace(Password) && PasswordValidationHelper.PasswordValidation(Password))
            {
                PasswordAttention = true;
                ReTypePasswordAttention = true;
                InfoMessage = $"{InfoMessage} {"Password (5-15 characters long, with at least one upper case letter. Do not use spaces.)"} ";
                IsValidationPropertiesProblem = true;
            }

            if (!string.IsNullOrWhiteSpace(Password) && Password.Contains(" "))
            {
                PasswordAttention = true;
                IsValidationPropertiesProblem = true;
            }

            if (!string.IsNullOrEmpty(Password) && Password != ReTypePassword)
            {
                PasswordAttention = true;
                ReTypePasswordAttention = true;
                InfoMessage = $"{InfoMessage} {"Password and Re-Password do not match."} ";
                IsValidationPropertiesProblem = true;
            }

            return IsValidationPropertiesProblem;
        }
    }
}
