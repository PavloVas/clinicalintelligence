﻿using ClinicalIntelligence.API;
using ClinicalIntelligence.Helpers;
using ClinicalIntelligence.Popups;
using ClinicalIntelligence.Views;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace ClinicalIntelligence.ViewModels
{
    public class LoginViewModel: BaseViewModel<ContentPage>
    {
        public ICommand LoginCommand { private set; get; }
        public ICommand SignUpCommand { private set; get; }
        public ICommand ForgotPasswordCommand { private set; get; }        

        private string _infoMessage;
		public string InfoMessage
		{
			get => _infoMessage;
		    set{ _infoMessage = value;
                OnPropertyChanged();
            }
		}

        private string _email;
        public string Email
        {
            get { return _email; }
            set { _email = value;
                EmailAttention = false;
                InfoMessage = string.Empty;
                OnPropertyChanged();
            }
        }

        private bool _emailAttention;
        public bool EmailAttention
        {
            get => _emailAttention;
            set{ _emailAttention = value;
                OnPropertyChanged();
            }
        }

        private string _password;
        public string Password
        {
            get { return _password; }
            set { _password = value;
                PasswordAttention = false;
                InfoMessage = string.Empty;
                OnPropertyChanged();
            }
        }

        private bool _passwordAttention;
        public bool PasswordAttention
        {
            get => _passwordAttention;
            set {  _passwordAttention = value;
                OnPropertyChanged();
            }
        }

        public LoginViewModel(LoginView view) : base(view)
        {
            Email = "esviatnii5";
            Password = "1111";

            LoginCommand = new Command(async () => 
            {
                SignInAsync();              
            });

            SignUpCommand = new Command(async () =>
            {
                await _view.Navigation.PushAsync(new SignUpView());
            });

            ForgotPasswordCommand = new Command(async () =>
                {
                    var popup = new ForgotPasswordPopup();
                    await PopupNavigation.PushAsync(popup);
                });

        }

        public async Task SignInAsync()
        {
            if (string.IsNullOrWhiteSpace(Email) && string.IsNullOrWhiteSpace(Password))
            {
                EmailAttention = true;
                PasswordAttention = true;
                InfoMessage = "Email and password are required";
                return;
            }

            if (string.IsNullOrWhiteSpace(Email))
            {
                EmailAttention = true;
                InfoMessage = "Email is required";
                return;
            }

            if (string.IsNullOrWhiteSpace(Password))
            {
                PasswordAttention = true;
                InfoMessage = "Password is required";
                return;
            }


            await CheckConnectionAsync();
            if (!HasInternetConnection)
            {
                return;
            }

            //if (EmailValidationHelper.EmailValidation(Email))
            //{
            //    EmailAttention = true;
            //    InfoMessage = "Email format is not correct.";
            //    return;
            //}

            ShowBusy("Authorization");

            var apiLoginResponse = await new ApiClient().LoginAsync(Email, Password);

            HideBusy();

            if (apiLoginResponse.Success)
            {
                Settings.UserEmail = Email;
                Settings.Token = apiLoginResponse.Token;
                Settings.PatientId = apiLoginResponse.PatientId;

                App.Current.MainPage = new MasterView() { };
            }
            else
            {
                //ShowMessagePopupAsync(apiLoginResponse.MessageError);
                InfoMessage = "Email and password are required";
                ShowMessagePopupAsync("The name or password is incorrect");
                EmailAttention = true;
                PasswordAttention = true;
            }
        }
    }
}
