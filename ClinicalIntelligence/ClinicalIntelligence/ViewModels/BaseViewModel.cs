﻿using Acr.UserDialogs;
using ClinicalIntelligence.Helpers;
using ClinicalIntelligence.Popups;
using ClinicalIntelligence.Views;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ClinicalIntelligence.ViewModels
{
    public class BaseViewModel<T> : INotifyPropertyChanged
    {

        public bool HasInternetConnection { get; set; }
        protected readonly T _view;
        public BaseViewModel(T view)
        {
            _view = view;
        }
      
        bool isBusy = false;
        public bool IsBusy
        {
            get { return isBusy; }
            set { SetProperty(ref isBusy, value); }
        }

        string title = string.Empty;
        public string Title
        {
            get { return title; }
            set { SetProperty(ref title, value); }
        }

        public void ShowBusy(string status = "tempdata")
        {
            if (status == "tempdata") status = "Loading...";

            IsBusy = true;

            Device.BeginInvokeOnMainThread(() =>
            {
                UserDialogs.Instance.ShowLoading(status);
            });
        }

        public void HideBusy()
        {
            IsBusy = false;

            Device.BeginInvokeOnMainThread(() =>
            {
                UserDialogs.Instance.HideLoading();
            });
        }

        public async Task CheckConnectionAsync()
        {
            HasInternetConnection = await ConnectivityHelper.HasConnection();
            if (!HasInternetConnection)
            {
                ShowMessagePopupAsync("Cannot perform this action while offline.");
            }
        }


        public async void ShowMessagePopupAsync(string message, string okText = null)
        {
            var popup = new MessagePopup(message, okText ?? "Ok");
            await PopupNavigation.PushAsync(popup);
        }

        protected bool SetProperty<T>(ref T backingStore, T value,
            [CallerMemberName]string propertyName = "",
            Action onChanged = null)
        {
            if (EqualityComparer<T>.Default.Equals(backingStore, value))
                return false;

            backingStore = value;
            onChanged?.Invoke();
            OnPropertyChanged(propertyName);
            return true;
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            var changed = PropertyChanged;
            if (changed == null)
                return;

            changed.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
