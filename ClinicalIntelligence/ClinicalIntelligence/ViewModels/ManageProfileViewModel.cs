﻿using ClinicalIntelligence.Views;
using Plugin.Media;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace ClinicalIntelligence.ViewModels
{
    public class ManageProfileViewModel : BaseViewModel<ContentPage>
    {
        public ICommand EditProfileCommand { private set; get; }
        public ICommand ChangeImageCommand { private set; get; }

        public ICommand UpdateCommand { private set; get; }
        public ICommand CancelCommand { private set; get; }


        private DateTime _DOB;
        public DateTime DOB
        {
            get => _DOB;
            set { _DOB = value;
                InfoMessage = string.Empty;
                OnPropertyChanged();
            }
        }

        private bool _DOBAttention;
        public bool DOBAttention
        {
            get => _DOBAttention;
            set
            {
                _DOBAttention = value;
                OnPropertyChanged();
            }
        }

        private string _consultingPhysicsan;
        public string ConsultingPhysicsan
        {
            get => _consultingPhysicsan;
            set { _consultingPhysicsan = value;
                InfoMessage = string.Empty;
                OnPropertyChanged();
            }
        }


        private bool _consultingPhysicsanAttention;
        public bool ConsultingPhysicsanAttention
        {
            get => _consultingPhysicsanAttention;
            set
            {
                _consultingPhysicsanAttention = value;
                OnPropertyChanged();
            }
        }

        private DateTime _previousAppointmentn;
        public DateTime PreviousAppointment
        {
            get => _previousAppointmentn;
            set { _previousAppointmentn = value;
                InfoMessage = string.Empty;
                OnPropertyChanged();
            }
        }

        private bool _previousAppointmentAttention;
        public bool PreviousAppointmentAttention
        {
            get => _previousAppointmentAttention;
            set
            {
                _previousAppointmentAttention = value;
                OnPropertyChanged();
            }
        }

        private DateTime _upcommingProcedures;
        public DateTime UpcommingProcedures
        {
            get => _upcommingProcedures;
            set { _upcommingProcedures = value;
                InfoMessage = string.Empty;
                OnPropertyChanged();
            }
        }

        private bool _upcommingProceduresAttentionAttention;
        public bool UpcommingProceduresAttention
        {
            get => _upcommingProceduresAttentionAttention;
            set
            {
                _upcommingProceduresAttentionAttention = value;
                OnPropertyChanged();
            }
        }

        private string _MPGPhysician;
        public string MPGPhysician
        {
            get => _MPGPhysician;
            set
            {
                _MPGPhysician = value;
                InfoMessage = string.Empty;
                OnPropertyChanged();
            }
        }

        private bool _MPGPhysicianAttention;
        public bool MPGPhysicianAttention
        {
            get => _MPGPhysicianAttention;
            set
            {
                _MPGPhysicianAttention = value;
                OnPropertyChanged();
            }
        }

        private string _referredBy;
        public string ReferredBy
        {
            get => _referredBy;
            set {  _referredBy = value;
                InfoMessage = string.Empty;
                OnPropertyChanged();
            }
        }

        private bool _referredByAttention;
        public bool ReferredByAttention
        {
            get => _referredByAttention;
            set
            {
                _referredByAttention = value;
                OnPropertyChanged();
            }
        }


        private string _payee;
        public string Payee
        {
            get => _payee;
            set { _payee = value;
                InfoMessage = string.Empty;
                OnPropertyChanged();
            }
        }

        private bool _payeeAttention;
        public bool PayeeAttention
        {
            get => _payeeAttention;
            set
            {
                _payeeAttention = value;
                OnPropertyChanged();
            }
        }


        private string _userPhoto;
        public string UserPhoto
        {
            get => _userPhoto;
            set
            {
                _userPhoto = value;
                InfoMessage = string.Empty;
                OnPropertyChanged();
            }
        }

        private bool _isEditProfil;
        public bool IsEditProfil
        {
            get => _isEditProfil;
            set
            {
                _isEditProfil = value;
                OnPropertyChanged();
            }
        }

        private string _infoMessage;
        public string InfoMessage
        {
            get { return _infoMessage; }
            set
            {
                _infoMessage = value;
                OnPropertyChanged();
            }
        }

        public ManageProfileViewModel(ManageProfileView view) : base(view)
        {
            InfoMessage = string.Empty;
            DOB = DateTime.Now.AddYears(-40); ;
            ConsultingPhysicsan = "Dr. Tom Bruce";
            PreviousAppointment = DateTime.Now;
            UpcommingProcedures = DateTime.Now; ;
            MPGPhysician = "Dr.Tom Bruce";
            ReferredBy = "Dr. Mark";
            Payee = "DVE";

            UserPhoto = "photo.png";
            IsEditProfil = false;


            EditProfileCommand = new Command(async () =>
            {

                IsEditProfil = true;
                //if (App.Current.MainPage is MasterDetailPage)
                //{
                //    _view.Navigation.PushAsync(new EditProfileView());
                //}
            });

            UpdateCommand = new Command(async () =>
            {
                IsEditProfil = false;
            });

            CancelCommand = new Command(async () =>
            {
                IsEditProfil = false;
            });


            ChangeImageCommand = new Command(async () =>
            {
                if (IsEditProfil)
                {
                    if (!CrossMedia.Current.IsPickPhotoSupported)
                    {
                        ShowMessagePopupAsync("Photos Not Supported;; Permission not granted to photos.", "OK");
                        return;
                    }
                    var file = await Plugin.Media.CrossMedia.Current.PickPhotoAsync(new Plugin.Media.Abstractions.PickMediaOptions
                    {
                        CompressionQuality = 75,
                        PhotoSize = Plugin.Media.Abstractions.PhotoSize.Medium
                    });

                    if (file == null)
                        return;

                    UserPhoto = file.Path;
                }
            });
        }
    }
}
