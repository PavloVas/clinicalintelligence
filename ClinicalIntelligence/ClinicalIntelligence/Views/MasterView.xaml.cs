﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ClinicalIntelligence.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MasterView : MasterDetailPage
    {
        public MasterView()
        {
            InitializeComponent();
            MasterPage.ListView.ItemSelected += ListView_ItemSelected;
        }

        private void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {


            var item = e.SelectedItem as MasterViewMenuItem;
            if (item == null)
                return;

            switch (item.NavigationPage)
            {
                case Models.NavigationMenuPage.Dashboard:
                    Detail = new NavigationPage(new DashboardView()) { BarBackgroundColor = Theme.COLOR_BROWN_BASE, BarTextColor = Color.White };
                    break;
                case Models.NavigationMenuPage.ManageProfile:
                    Detail = new NavigationPage(new ManageProfileView()) { BarBackgroundColor = Theme.COLOR_BROWN_BASE, BarTextColor = Color.White };
                    break;
                case Models.NavigationMenuPage.Questionnair:
                    Detail = new NavigationPage(new QuestionnaireView()) { BarBackgroundColor = Theme.COLOR_BROWN_BASE, BarTextColor = Color.White };
                    break;
                case Models.NavigationMenuPage.Appointments:
                    Detail = new NavigationPage(new AppointmentsView()) { BarBackgroundColor = Theme.COLOR_BROWN_BASE, BarTextColor = Color.White };
                    break;
                case Models.NavigationMenuPage.LogOut:
                    Settings.Token = "";
                    App.Current.MainPage = new NavigationPage(new LoginView());
                    break;
                default:
                    break;
            }          
           
            IsPresented = false;
            MasterPage.ListView.SelectedItem = null;
        }
    }
}