﻿using ClinicalIntelligence.ViewModels;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ClinicalIntelligence.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SignUpView : ContentPage
	{
		public SignUpView ()
		{			
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, false);
            this.BindingContext = new SignUpViewModel(this);
        }
	}
}