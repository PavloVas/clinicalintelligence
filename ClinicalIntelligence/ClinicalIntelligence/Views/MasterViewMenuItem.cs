﻿using ClinicalIntelligence.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClinicalIntelligence.Views
{

    public class MasterViewMenuItem
    {       
        public int Id { get; set; }
        public string Title { get; set; }
        public string Icon { get; set; } 

        public NavigationMenuPage NavigationPage { get; set; }
    }
}