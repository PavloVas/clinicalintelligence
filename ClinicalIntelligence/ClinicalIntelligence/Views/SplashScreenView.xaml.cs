﻿using ClinicalIntelligence.SplashScreenAnimation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ClinicalIntelligence.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SplashScreenView : ContentPage
	{
		public SplashScreenView ()
		{
			InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, false);
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();

            //imageview let him offset 50, decrement 10 each time, jump 5 times
            await Animate.BallAnimate(this.logoImage, 50, 10, 5);

            /// / Animation shows the end of the home page)
           // await Navigation.PushModalAsync(new LoginView());

            App.Current.MainPage = new NavigationPage(new LoginView());

        }
    }
}