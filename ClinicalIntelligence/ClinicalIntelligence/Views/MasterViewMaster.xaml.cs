﻿using ClinicalIntelligence.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ClinicalIntelligence.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MasterViewMaster : ContentPage
    {
        public ListView ListView;

        public MasterViewMaster()
        {
            InitializeComponent();
            userName.Text = Settings.UserEmail;
            BindingContext = new MasterViewMasterViewModel();
            ListView = MenuItemsListView;           
        }

        class MasterViewMasterViewModel : INotifyPropertyChanged
        {
            public ObservableCollection<MasterViewMenuItem> MenuItems { get; set; }
            
            public MasterViewMasterViewModel()
            {
               
                MenuItems = new ObservableCollection<MasterViewMenuItem>(new[]
                {
                    new MasterViewMenuItem { Id = 0, Icon="ic_dashboard", Title = "Dashboard", NavigationPage = NavigationMenuPage.Dashboard},
                    new MasterViewMenuItem { Id = 1, Icon="ic_manageProfil", Title = "Manage Profile", NavigationPage = NavigationMenuPage.ManageProfile},
                    new MasterViewMenuItem { Id = 2, Icon="ic_questionnaire", Title = "Questionnaire", NavigationPage = NavigationMenuPage.Questionnair },
                    new MasterViewMenuItem { Id = 3, Icon="", Title = "Appointments", NavigationPage = NavigationMenuPage.Appointments},
                    new MasterViewMenuItem { Id = 4, Icon="", Title = "LogOut", NavigationPage = NavigationMenuPage.LogOut},
                });
            }
            
            #region INotifyPropertyChanged Implementation
            public event PropertyChangedEventHandler PropertyChanged;
            void OnPropertyChanged([CallerMemberName] string propertyName = "")
            {
                if (PropertyChanged == null)
                    return;

                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
            #endregion
        }
    }
}