﻿using ClinicalIntelligence.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ClinicalIntelligence.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ManageProfileView : ContentPage
	{
		public ManageProfileView ()
		{
			InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, false);
            BindingContext = new ManageProfileViewModel(this);
        }
	}
}