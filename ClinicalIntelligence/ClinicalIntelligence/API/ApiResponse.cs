﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClinicalIntelligence.API
{
    public class ApiResponse
    {
        public string Error { get; set; }
        [JsonIgnore]
        public string MessageError { get; set; }
        [JsonIgnore]
        public bool Success
        {
            get
            {
                return string.IsNullOrWhiteSpace(Error) && string.IsNullOrWhiteSpace(MessageError);
            }
        }
    }
}
