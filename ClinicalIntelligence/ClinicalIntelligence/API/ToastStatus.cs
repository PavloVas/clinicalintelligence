﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClinicalIntelligence.API
{
    public enum ToastStatus
    {
        Default,
        Success,
        Error,
        DeletedEntry,
        SubmittedEntry,
        RevertedEntry,
        RejectedEntry,
        ApprovedEntry

    }
}
