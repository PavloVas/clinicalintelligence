﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClinicalIntelligence.API
{
    public class SignUpArgs
    {
        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("patientID")]
        public string PatientID { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }
    }
}
