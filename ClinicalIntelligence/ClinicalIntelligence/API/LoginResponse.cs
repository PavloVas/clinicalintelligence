﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClinicalIntelligence.API
{
    public class LoginResponse : ApiResponse
    {
        [JsonProperty("patientId")]
        public string PatientId { get; set; }

        [JsonProperty("token")]
        public string Token { get; set; }
    }
}
