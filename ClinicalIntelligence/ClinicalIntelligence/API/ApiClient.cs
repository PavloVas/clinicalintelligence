﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ClinicalIntelligence.API
{
   public class ApiClient
    {
        private const string API_BASE_URL = "http://api.clinicalintelligence.com.au/api/Users/";
        public async Task<LoginResponse> LoginAsync(string username, string password)
        {
            return await TryInvoceAsync<LoginResponse>(async () =>
            {
                var httpClient = GetHttpClient();
                var json = SerializeToJson(new LoginArgs
                {
                    UserName = username,
                    Password = password
                });
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                var result = await httpClient.PostAsync("PostGetToken", content);
                return result;
               
            });
        }

        public async Task<string> PostGenerateUsernameAsync(SignUpArgs args)
        {
            var httpClient = GetHttpClient();
            var json = SerializeToJson(args);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await httpClient.PostAsync("PostGenerateUsername", content);
            string username = await response.Content.ReadAsStringAsync();
            return username;

            // return await TryInvoceAsync<PostGenerateUsernameResponse>(async () =>
            //{
            //var httpClient = GetHttpClient();
            //var json = SerializeToJson(args);
            //var content = new StringContent(json, Encoding.UTF8, "application/json");
            //var response = await httpClient.PostAsync("/Users/PostGenerateUsername", content);
            //string username = await response.Content.ReadAsStringAsync();
            //return username;
            // });
        }


        public async Task<SignUpResponse> SignUpAsync(SignUpArgs args)
        {
            return await TryInvoceAsync<SignUpResponse>(async () =>
            {
                var httpClient = GetHttpClient();
                var json = SerializeToJson(args);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                var result = await httpClient.PostAsync("PostSignUp", content);
                return result;
            });
        }

        public async Task<SignUpResponse> GetAllUsers(SignUpArgs args)
        {
            return await TryInvoceAsync<SignUpResponse>(async () =>
            {
                var httpClient = GetHttpClient();
              //  var json = SerializeToJson(args);
                //var content = new StringContent(json, Encoding.UTF8, "application/json");
                var result = await httpClient.GetAsync("GetAll");
                return result;
            });
        }


        #region Helper methods    


        private HttpClient GetHttpClient(bool useHttps = false)
        {
           // var url = useHttps ? API_BASE_HTTPS_URL : API_BASE_URL;

            var httpClient = new HttpClient
            {
                BaseAddress = new Uri(API_BASE_URL),
                Timeout = TimeSpan.FromSeconds(25)
            };
             return httpClient;
        }

        private string SerializeToJson(object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }

        private static async Task<T> TryInvoceAsync<T>(Func<Task<HttpResponseMessage>> func) where T : ApiResponse, new()
        {
            try
            {
                var responseMessage = await func();

               // Settings.ServerDateTime = responseMessage.Headers.Date.ToString();
                if (!responseMessage.IsSuccessStatusCode)
                {
                    var msg = await responseMessage.Content.ReadAsStringAsync();

                    return
                        GenerateErroResponse<T>(
                            $"API respons error: <{responseMessage.ReasonPhrase}> when try get request from {responseMessage.RequestMessage.RequestUri} with method {responseMessage.RequestMessage.Method} with error message {msg}", msg.Replace("{\"Error\":\"", "").Replace("\"}", ""));
                }

                var responseContext = await responseMessage.Content.ReadAsStringAsync();

                var deserializeData = JsonConvert.DeserializeObject<T>(responseContext);
                return deserializeData;
            }
            catch (Exception ex)
            {
                return GenerateErroResponse<T>(ex.Message, "");
            }
        }

        private static T GenerateErroResponse<T>(string errorMesage, string serverMessageError) where T : ApiResponse, new()
        {
            var errorResult = new T();
            errorResult.Error = errorMesage;
            errorResult.MessageError = serverMessageError;

            if (errorResult.MessageError == "")
            {
                errorResult.MessageError = errorResult.Error;
            }

            if (errorResult.MessageError.Contains("NameResolutionFailure"))
            {
                errorResult.MessageError = "Cannot perform this action while offline.";
            }

            return errorResult;
        }
        #endregion
    }
}
