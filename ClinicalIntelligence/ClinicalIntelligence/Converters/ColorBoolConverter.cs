﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace ClinicalIntelligence.Converters
{
    public class ColorBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (parameter != null)
            {
                if (bool.Parse(value.ToString()) == true)
                {
                    return Theme.COLOR_GRAY_BASE;
                }

                return Theme.COLOR_ORANGE_BASE;
            }
            else
            {

                if (bool.Parse(value.ToString()) == true)
                {
                    return Theme.COLOR_ORANGE_BASE;
                }

                return Theme.COLOR_GRAY_BASE;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
