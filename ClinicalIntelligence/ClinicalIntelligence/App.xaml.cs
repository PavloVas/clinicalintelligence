﻿using System;
using Xamarin.Forms;
using ClinicalIntelligence.Views;
using Xamarin.Forms.Xaml;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;

[assembly: XamlCompilation (XamlCompilationOptions.Compile)]
namespace ClinicalIntelligence
{
	public partial class App : Application
	{
		
		public App ()
		{
			InitializeComponent();

            //MainPage = new NavigationPage(new SplashScreenView());
            //MainPage = new NavigationPage(new LoginView()); // ;
            // MainPage = new NavigationPage(new SignUpView());

            if (string.IsNullOrEmpty(Settings.Token))            
               MainPage = new NavigationPage(new SplashScreenView());            
            else
                MainPage = new MasterView();
        }

		protected override void OnStart ()
		{
            // Handle when your app starts
            AppCenter.Start("ios=c57aca07-31c6-4390-a45f-403404d089bb;",
                  typeof(Analytics), typeof(Crashes));
        }

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
