﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace ClinicalIntelligence.Models
{
   public class QuestionnaireModel 
    {
        public int Id { get; set; }
        public string HealthSurvey { get; set; }
        public string ClinicName { get; set; }
        public QuestionnairesStatus Status { get; set; }
    }
}
