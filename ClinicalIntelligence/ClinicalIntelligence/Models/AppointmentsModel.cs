﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace ClinicalIntelligence.Models
{
    public class AppointmentsModels : ObservableCollection<AppointmentsData>
    {
        public string GroupName{ get; set; }
        public ObservableCollection<AppointmentsData> AppointmentsData => this;
    }

    public class AppointmentsData
    {
        public string  Image { get; set; }
        public string Title { get; set; }
        public string Doctor { get; set; }
        public string ReceptionTime { get; set; }
    }
}
