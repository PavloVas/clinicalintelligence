﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClinicalIntelligence.Models
{
    public class CheckedArgs
    {
        public bool IsChecked { get; set; }
        public object Parameter { get; set; }
    }
}
