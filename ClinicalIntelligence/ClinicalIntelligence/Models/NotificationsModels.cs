﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClinicalIntelligence.Models
{
    public class NotificationsModels
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Qualification { get; set; }
        public string Image { get; set; }        
    }
}
