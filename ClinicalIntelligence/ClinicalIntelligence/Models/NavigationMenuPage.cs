﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClinicalIntelligence.Models
{
    public enum NavigationMenuPage
    {
        Dashboard,
        ManageProfile,
        Questionnair,
        Appointments,
        LogOut
    }
}
