﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClinicalIntelligence.Models
{
    public enum QuestionnairesStatus
    {
        NEW,
        PENDING,
        FILLED
    }
}
