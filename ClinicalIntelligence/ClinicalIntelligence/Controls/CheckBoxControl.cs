﻿using ClinicalIntelligence.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace ClinicalIntelligence.Controls
{
    public class CheckBoxControl : Button
    {
        private string _checkImage = Device.RuntimePlatform == Device.UWP ? "Images/ic_checkBoxTrue.png" : "ic_checkBoxTrue.png";
        private string _unCheckImage = Device.RuntimePlatform == Device.UWP ? "Images/ic_checkBoxFalse.png" : "ic_checkBoxFalse.png";

        public static readonly BindableProperty IsCheckedProperty = BindableProperty.Create("IsChecked", typeof(bool),  typeof(CheckBoxControl), false, defaultBindingMode: BindingMode.TwoWay, propertyChanging: CheckChanging);
        public static readonly BindableProperty CheckOnBindOnlyProperty = BindableProperty.Create("CheckOnBindOnly", typeof(bool), typeof(CheckBoxControl), false);
        public static readonly BindableProperty ItemCheckedCommandProperty = BindableProperty.Create("ItemCheckedCommand", typeof(object), typeof(CheckBoxControl), false, propertyChanging: null);
        public static readonly BindableProperty CommandParameterProperty = BindableProperty.Create("CommandParameter", typeof(object), typeof(CheckBoxControl), false, propertyChanging: null);

        public bool IsChecked
        {
            get { return (bool)GetValue(IsCheckedProperty); }
            set { this.SetValue(IsCheckedProperty, value); }
        }

        public bool CheckOnBindOnly
        {
            get { return (bool)GetValue(CheckOnBindOnlyProperty); }
            set { this.SetValue(CheckOnBindOnlyProperty, value); }
        }

        public object ItemCheckedCommand
        {
            get { return (object)GetValue(ItemCheckedCommandProperty); }
            set { SetValue(ItemCheckedCommandProperty, value); }
        }

        public object CommandParameter
        {
            get { return (object)GetValue(CommandParameterProperty); }
            set { SetValue(CommandParameterProperty, value); }
        }

        public CheckBoxControl()
        {
            //var tapGestureRecognizer = new TapGestureRecognizer();
            //tapGestureRecognizer.Tapped += TapGestureRecognizer_Tapped;
            //GestureRecognizers.Add(tapGestureRecognizer);

            Clicked += TapGestureRecognizer_Tapped;

            BackgroundColor = Color.Transparent;

            this.Image = _unCheckImage;
        }

        private static void CheckChanging(object bindable, object oldValue, object newValue)
        {
            if (newValue == oldValue)
            {
                return;
            }

            var checkBox = bindable as CheckBoxControl;

            if (newValue is bool)
            {
                checkBox.Image = (bool)newValue == true ? checkBox._checkImage : checkBox._unCheckImage;
            }
        }

        private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            var checkBox = sender as CheckBoxControl;

            if (!checkBox.CheckOnBindOnly)
            {
                IsChecked = !IsChecked;
            }

            var context = checkBox.BindingContext;
            var command = (checkBox.ItemCheckedCommand as Command<CheckedArgs>);

            ItemChecked?.Invoke(this, null);

            if (command != null)
            {
                command.Execute(new CheckedArgs() { IsChecked = IsChecked, Parameter = context });
            }
        }

        public event EventHandler ItemChecked;
    }
}
