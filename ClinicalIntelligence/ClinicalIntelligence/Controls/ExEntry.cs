﻿using Xamarin.Forms;

namespace ClinicalIntelligence.Controls
{
    public class ExEntry : Entry
    {
        public static readonly BindableProperty IsReadOnlyProperty = BindableProperty.Create("IsReadOnly", typeof(bool), typeof(ExEntry), false, BindingMode.OneWay, propertyChanged: IsReadOnlyChanged);
        public static readonly BindableProperty MaxLengthProperty = BindableProperty.Create("MaxLength", typeof(int?), typeof(ExEntry), null, BindingMode.OneWay);
        public static readonly BindableProperty IsNumericProperty = BindableProperty.Create("IsNumeric", typeof(bool), typeof(ExEntry), false, BindingMode.OneWay, propertyChanged: null);

        public bool IsReadOnly
        {
            get { return (bool)GetValue(IsReadOnlyProperty); }
            set { this.SetValue(IsReadOnlyProperty, value); }
        }

        public int? MaxLength
        {
            get { return (int?)GetValue(MaxLengthProperty); }
            set { this.SetValue(MaxLengthProperty, value); }
        }

        public bool IsNumeric
        {
            get { return (bool)GetValue(IsNumericProperty); }
            set { this.SetValue(IsNumericProperty, value); }
        }

        private static void IsReadOnlyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var control = bindable as ExEntry;
            if ((bool)newValue)
            {
                control.BackgroundColor = Theme.COLOR_PAGE_BACKGROUND;
                control.TextColor = Theme.COLOR_TEXT_GRAY;
            }
            else
            {
                control.BackgroundColor = Color.White;
                control.TextColor = Color.Black;
            }
        }

        public ExEntry()
        {
            TextChanged += (sender, e) =>
            {
                if (!MaxLength.HasValue)
                {
                    return;
                }

                string text = Text;      //Get Current Text
                if (text.Length > MaxLength)       //If it is more than your character restriction
                {
                    text = text.Remove(text.Length - 1);  // Remove Last character
                    Text = text;        //Set the Old value
                }
            };
        }
    }
}
