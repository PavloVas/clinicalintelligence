﻿using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace ClinicalIntelligence.Controls
{
    public class ExEntryControl : StackLayout
    {
        private readonly Label _labelHint;
        private readonly Entry _entryText;

        public event EventHandler FocusLost;
        public event EventHandler FocusedGot;
        public event EventHandler EntryInputCompleted;

        public static readonly BindableProperty IsPasswordProperty = BindableProperty.Create("IsPassword", typeof(bool),
            typeof(ExEntryControl), default(bool),
            propertyChanged: (bindable, oldvalue, newvalue) =>
                ((ExEntryControl)bindable)._entryText.IsPassword = (bool)newvalue);

        public static readonly BindableProperty TextProperty = BindableProperty.Create<ExEntryControl, string>(
            n => n.Text, default(string), BindingMode.TwoWay, null,
            (bindable, oldvalue, newvalue) => ((ExEntryControl)bindable)._entryText.Text = (string)newvalue);


        public static readonly BindableProperty TextColorProperty = BindableProperty.Create("TextColor", typeof(Color),
            typeof(ExEntryControl), Color.Black,
            propertyChanged: (bindable, oldvalue, newvalue) =>
                ((ExEntryControl)bindable)._entryText.TextColor = (Color)newvalue);

        public static readonly BindableProperty PlaceholderProperty = BindableProperty.Create("Placeholder",
            typeof(string), typeof(ExEntryControl), default(string),
            propertyChanged: (bindable, oldvalue, newvalue) =>
                ((ExEntryControl)bindable)._entryText.Placeholder = (string)newvalue);


        public static readonly BindableProperty KeyboardProperty = BindableProperty.Create("Keyboard", typeof(Keyboard),
            typeof(ExEntryControl), Keyboard.Default,
            propertyChanged: (bindable, oldvalue, newvalue) =>
                ((ExEntryControl)bindable)._entryText.Keyboard = (Keyboard)newvalue);

        public static readonly BindableProperty BackgroundColorProperty = BindableProperty.Create("BackgroundColor",
            typeof(Color), typeof(ExEntryControl), Color.White,
            propertyChanged: (bindable, oldvalue, newvalue) =>
                ((ExEntryControl)bindable)._entryText.BackgroundColor = (Color)newvalue);

        public static readonly BindableProperty BackgroundColorAttentionProperty =
            BindableProperty.Create("BackgroundColorAttention", typeof(Color), typeof(ExEntryControl),
                Theme.COLOR_ERROR_BACKGROUND_RED);

        public static readonly BindableProperty HeaderProperty = BindableProperty.Create("Header", typeof(string),
            typeof(ExEntryControl), default(string),
            propertyChanged: (bindable, oldvalue, newvalue) =>
                ((ExEntryControl)bindable)._labelHint.Text = (string)newvalue);

        public static readonly BindableProperty HeaderTextColorProperty = BindableProperty.Create("HeaderTextColor",
            typeof(Color), typeof(ExEntryControl), Theme.COLOR_TEXT_GRAY,
            propertyChanged: (bindable, oldvalue, newvalue) =>
                ((ExEntryControl)bindable)._labelHint.TextColor = (Color)newvalue);

        public static readonly BindableProperty HeaderTextAttentionColorProperty =
            BindableProperty.Create("HeaderTextAttentionColor", typeof(Color), typeof(ExEntryControl),
                Theme.COLOR_ERROR_TEXT_RED);

        public static readonly BindableProperty HeaderStyleProperty = BindableProperty.Create("HeaderStyle",
            typeof(Style), typeof(ExEntryControl), LabelEntryTitleStyle,
            propertyChanged: (bindable, oldvalue, newvalue) =>
                ((ExEntryControl)bindable)._labelHint.Style = (Style)newvalue);

        public static readonly BindableProperty HasAttentionProperty = BindableProperty.Create("HasAttention",
            typeof(bool), typeof(ExEntryControl), default(bool),
            propertyChanged: HasAttentionPropertyChanged);

        public static readonly BindableProperty HeaderFormattedTextProperty = BindableProperty.Create(
            "HeaderFormattedText", typeof(FormattedString), typeof(ExEntryControl), default(FormattedString),
            propertyChanged: (bindable, oldvalue, newvalue) =>
                ((ExEntryControl)bindable)._labelHint.FormattedText = (FormattedString)newvalue);

        public static readonly BindableProperty FocusedCommandProperty =
            BindableProperty.Create("FocusedCommand", typeof(ICommand), typeof(ExEntryControl));

        public static Style LabelEntryTitleStyle;

        static ExEntryControl()
        {
            LabelEntryTitleStyle = (Style)Application.Current.Resources["labelEntryTitleStyle"];
        }

        public ICommand FocusedCommand
        {
            get => (ICommand)GetValue(FocusedCommandProperty);
            set => SetValue(FocusedCommandProperty, value);
        }

        public string Header
        {
            get => (string)GetValue(HeaderProperty);
            set => SetValue(HeaderProperty, value);
        }

        public FormattedString HeaderFormattedText
        {
            get => (FormattedString)GetValue(HeaderFormattedTextProperty);
            set => SetValue(HeaderFormattedTextProperty, value);
        }

        public bool HasAttention
        {
            get => (bool)GetValue(HasAttentionProperty);
            set => SetValue(HasAttentionProperty, value);
        }

        public Style HeaderStyle
        {
            get => (Style)GetValue(HeaderStyleProperty);
            set => SetValue(HeaderStyleProperty, value);
        }


        public Color BackgroundColor
        {
            get => (Color)GetValue(BackgroundColorProperty);
            set => SetValue(BackgroundColorProperty, value);
        }

        public Color BackgroundColorAttention
        {
            get => (Color)GetValue(BackgroundColorAttentionProperty);
            set => SetValue(BackgroundColorAttentionProperty, value);
        }

        public Color HeaderTextColor
        {
            get => (Color)GetValue(HeaderTextColorProperty);
            set => SetValue(HeaderTextColorProperty, value);
        }

        public Color HeaderTextAttentionColor
        {
            get => (Color)GetValue(HeaderTextAttentionColorProperty);
            set => SetValue(HeaderTextAttentionColorProperty, value);
        }

        public Keyboard Keyboard
        {
            get => (Keyboard)GetValue(KeyboardProperty);
            set => SetValue(KeyboardProperty, value);
        }

        public bool IsPassword
        {
            get => (bool)GetValue(IsPasswordProperty);
            set => SetValue(IsPasswordProperty, value);
        }

        public string Text
        {
            get => (string)GetValue(TextProperty);
            set => SetValue(TextProperty, value);
        }

        public string Placeholder
        {
            get => (string)GetValue(PlaceholderProperty);
            set => SetValue(PlaceholderProperty, value);
        }

        public Color TextColor
        {
            get => (Color)GetValue(TextColorProperty);
            set => SetValue(TextColorProperty, value);
        }

        public ExEntryControl()
        {
            _labelHint = new Label();
            _entryText = new Entry();

            _labelHint.Style = LabelEntryTitleStyle;

           // Children.Add(_labelHint);
            Children.Add(_entryText);

            _entryText.BackgroundColor = Color.White;
            _entryText.TextChanged += (sender, e) => { Text = _entryText.Text; };

            _entryText.Focused += (sender, e) =>
            {
                var context = BindingContext;
                var command = FocusedCommand;
                command?.Execute(context);
            };

            _entryText.Focused += (sender, e) => { FocusedGot?.Invoke(this, null); };

            _entryText.Unfocused += (sender, e) => { FocusLost?.Invoke(this, null); };

            if (Device.RuntimePlatform == Device.UWP)
            {
                _entryText.Completed += (sender, args) => { EntryInputCompleted?.Invoke(sender, args); };
            }
        }

        private static void HasAttentionPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var entryCustom = ((ExEntryControl)bindable);

            if (oldValue != newValue)
            {
                if (newValue is bool b)
                {
                    if (b)
                    {
                        entryCustom._labelHint.TextColor = entryCustom.HeaderTextAttentionColor;
                        entryCustom._entryText.BackgroundColor = entryCustom.BackgroundColorAttention;
                    }
                    else
                    {
                        entryCustom._labelHint.TextColor = entryCustom.HeaderTextColor;
                        entryCustom._entryText.BackgroundColor = entryCustom.BackgroundColor;
                    }
                }
            }
        }
    }
}