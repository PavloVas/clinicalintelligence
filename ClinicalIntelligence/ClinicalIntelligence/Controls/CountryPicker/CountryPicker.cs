﻿using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using XamCountryPicker.Models;

namespace XamCountryPicker
{
   public class CountryPicker: Entry
    {       
        public static readonly BindableProperty CommandProperty = BindableProperty.Create("Command", typeof(ICommand), typeof(CountryPicker), null, propertyChanged: (bo, o, n) => ((CountryPicker)bo).OnCommandChanged());
        
        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }


        public CountryPicker()
        {
            Placeholder = "Select Country";
            Focused += (q, w) => { ShowPopupAsync(); };           
        }
              

        public async System.Threading.Tasks.Task ShowPopupAsync()
        {
            var popup = new CountriesPickerView();
            await PopupNavigation.PushAsync(popup);

            popup.CloseEvent += Popup_CloseEvent;
        }

        void OnCommandChanged()
        {
            //ICommand cmd = Command;
            //if (cmd != null)
            //    cmd.Execute(null);
        }

        private void Popup_CloseEvent(object sender, EventArgs e)
        {
            ICommand cmd = Command;
            if (cmd != null)
                cmd.Execute(((Country)sender));

            Text = ((Country)sender).Name;
            Unfocus();
        }
    }
}
