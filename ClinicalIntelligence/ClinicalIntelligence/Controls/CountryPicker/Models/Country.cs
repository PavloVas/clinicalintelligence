﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XamCountryPicker.Models
{
    public class Country
    {       
        public string Code { get; private set; }
        public string Name { get; private set; }
        public string DialCode { get; private set; }
        public string Flag { get; private set; }
        public string Currency { get; private set; }


        public Country(string code, string name, string dialCode, string flag_ad, string currency)
        {
            Code = code;
            Name = name;
            DialCode = dialCode;
            Flag = flag_ad;
            Currency = currency;
        }

        
    }
}