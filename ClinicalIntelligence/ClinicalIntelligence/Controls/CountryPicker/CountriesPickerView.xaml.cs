﻿using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamCountryPicker.Models;

namespace XamCountryPicker
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CountriesPickerView : PopupPage
    {
        public event EventHandler CloseEvent;
        private ObservableCollection<Country> _countriesListSourted;
        public ObservableCollection<Country> CountriesListSourted
        {
            get
            {
                return _countriesListSourted;
            }
            set
            {
                _countriesListSourted = value;
                OnPropertyChanged();
            }
        }

        public List<Country> CountriesList
        {
            get;
            set;
        }


        public ICommand SelectedItemCommand { private set; get; }
        

        //private string _textSoutre;
        //public string TextSoutre
        //{
        //    get
        //    {
        //        return _textSoutre;
        //    }
        //    set
        //    {
        //        _textSoutre = value;
        //        if (!string.IsNullOrEmpty(value))
        //        {
        //            CountriesListSourted = CountriesList.Sort((s1, s2) => s1.Name.CompareTo(s2));
        //        }
        //        OnPropertyChanged();
        //    }
        //}


        public CountriesPickerView()
        {
            InitializeComponent();
            BindingContext = this;
            CountriesList = new Countries().CountriesList;

            CountriesListV.ItemSelected += async (object sender, SelectedItemChangedEventArgs e) => 
            {
                var listView = ((ListView)sender);
                if (CloseEvent != null)
                    CloseEvent.Invoke(((Country)listView.SelectedItem), EventArgs.Empty);
                await PopupNavigation.PopAsync();
            };           
        }    
    }
}