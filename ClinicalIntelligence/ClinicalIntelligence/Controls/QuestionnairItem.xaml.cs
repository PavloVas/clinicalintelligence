﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ClinicalIntelligence.Controls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class QuestionnairItem : ContentView
    {
        public static readonly BindableProperty IsFocusedProperty = BindableProperty.Create("IsFocused", typeof(bool), typeof(VisualElement), false,
            
            propertyChanging: (bindable, oldvalue, newvalue) => 
            {
            //((QuestionnairItem)bindable).OnIsVisibleChanged((bool)oldvalue, (bool)newvalue))


            });
               

        public bool IsFocused
        {
            get { return (bool)GetValue(IsVisibleProperty); }
            set { SetValue(IsVisibleProperty, value); }
        }


        public QuestionnairItem()
        {
            InitializeComponent();
            var tap = new TapGestureRecognizer();
            tap.Tapped += (w, e) =>
            {
                if (IsFocused)
                {
                    secondPanel1.IsVisible = false;
                    secondPanel2.IsVisible = true;
                }
                else
                {
                    secondPanel1.IsVisible = true;
                    secondPanel2.IsVisible = false;
                }
            };
            // arrowhead_right.GestureRecognizers.Add(tap);
            var eee = arrowhead_right.GestureRecognizers;

        }
    }
}