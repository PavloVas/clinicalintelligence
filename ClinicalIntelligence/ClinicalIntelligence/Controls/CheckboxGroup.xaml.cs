﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ClinicalIntelligence.Controls
{

    public enum AppointmentsSort
    {
        Today,
        Past,
        Future
    }


	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CheckboxGroup : ContentView, INotifyPropertyChanged
    {
        public ICommand CheckBox1Command { private set; get; }
        public ICommand CheckBox2Command { private set; get; }
        public ICommand CheckBox3Command { private set; get; }

        public static readonly BindableProperty CheckBoxSelectProperty =
                                BindableProperty.Create<CheckboxGroup, int>(p => p.CheckBoxSelect, 0, BindingMode.TwoWay, propertyChanging: CheckBoxSelectPropertyChanging);

        private static void CheckBoxSelectPropertyChanging(BindableObject bindable, int oldValue, int newValue)
        {
            if (bindable is CheckboxGroup)
            {
                switch (newValue)
                {
                    case(int) AppointmentsSort.Today:
                        (bindable as CheckboxGroup).CheckBox1 = true;
                        break;
                    case (int)AppointmentsSort.Past:
                        (bindable as CheckboxGroup).CheckBox2 = true;
                        break;
                    case (int)AppointmentsSort.Future:
                        (bindable as CheckboxGroup).CheckBox3 = true;
                        break;
                    default:
                        break;
                }
            }
        }

        public int CheckBoxSelect
        {
            get { return (int)base.GetValue(CheckBoxSelectProperty); }
            set { base.SetValue(CheckBoxSelectProperty, value); }
        }


        private bool _checkBox1;
        public bool CheckBox1
        {
            get { return _checkBox1; }
            set
            {
                _checkBox1 = value;
                if (value)
                {
                    CheckBox2 = false;
                    CheckBox3 = false;

                    CheckBoxSelect = (int)AppointmentsSort.Today;
                }
                OnPropertyChanged();
            }
        }


        private bool _checkBox2;
        public bool CheckBox2
        {
            get { return _checkBox2; }
            set
            {
                _checkBox2 = value;
                if (value)
                {
                    CheckBox1 = false;
                    CheckBox3 = false;

                    CheckBoxSelect = (int)AppointmentsSort.Past;
                }
                OnPropertyChanged();
            }
        }

        private bool _checkBox3;
        public bool CheckBox3
        {
            get { return _checkBox3; }
            set
            {
                _checkBox3 = value;
                if (value)
                {
                    CheckBox1 = false;
                    CheckBox2 = false;

                    CheckBoxSelect = (int)AppointmentsSort.Future;
                }
                OnPropertyChanged();
            }
        }


        public CheckboxGroup ()
		{
			InitializeComponent ();
            BindingContext = this;
            CheckBox1 = true;

            CheckBox1Command = new Command(async () =>
            {
                CheckBox1 = true;                
            });
            CheckBox2Command = new Command(async () =>
            {
                CheckBox2 = true;               
            });
            CheckBox3Command = new Command(async () =>
            {
                CheckBox3 = true;               
            });
        }

        protected bool SetProperty<T>(ref T backingStore, T value,
            [CallerMemberName]string propertyName = "",
            Action onChanged = null)
        {
            if (EqualityComparer<T>.Default.Equals(backingStore, value))
                return false;

            backingStore = value;
            onChanged?.Invoke();
            OnPropertyChanged(propertyName);
            return true;
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            var changed = PropertyChanged;
            if (changed == null)
                return;

            changed.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}