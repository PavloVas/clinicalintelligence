﻿using ClinicalIntelligence.ViewModels;
using ClinicalIntelligence.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ClinicalIntelligence.Controls
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HeaderControl : ContentView
	{

        public static readonly BindableProperty TextProperty = BindableProperty.Create<HeaderControl, string>(p => p.Text, "", BindingMode.OneWay, propertyChanging: TextChanging);
        public static readonly BindableProperty NotificationCountsProperty = BindableProperty.Create<HeaderControl, string>(p => p.NotificationCounts, "", BindingMode.OneWay, propertyChanging: NotificationCountsChanging);
        public static readonly BindableProperty PendingCountsProperty = BindableProperty.Create<HeaderControl, string>(p => p.PendingCounts, "", BindingMode.OneWay, propertyChanging: PendingCountsChanging);
        public static readonly BindableProperty NotificationPanelProperty = BindableProperty.Create<HeaderControl, bool>(p => p.NotificationPanel,true, BindingMode.OneWay, propertyChanging: NotificationPanelChanging);
        public static readonly BindableProperty IsShowLogoProperty = BindableProperty.Create<HeaderControl, bool>(p => p.IsShowLogo, false, BindingMode.OneWay, propertyChanging: IsShowLogoChanging);

       

        public HeaderControl ()
		{
			InitializeComponent ();
             var gesture =  new TapGestureRecognizer();
            imgHomeIcon.GestureRecognizers.Add(gesture);

            gesture.Tapped += ShowMenuOrGoBack;
        }
       

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public bool IsShowLogo
        {
            get { return (bool)GetValue(IsShowLogoProperty); }
            set { SetValue(IsShowLogoProperty, value); }
        }


        public string NotificationCounts
        {
            get { return (string)GetValue(NotificationCountsProperty); }
            set { SetValue(NotificationCountsProperty, value); }
        }

        public string PendingCounts
        {
            get { return (string)GetValue(NotificationCountsProperty); }
            set { SetValue(NotificationCountsProperty, value); }
        }

        public bool NotificationPanel
        {
            get { return (bool)GetValue(NotificationPanelProperty); }
            set { SetValue(NotificationPanelProperty, value); }
        }
               

         private static void NotificationPanelChanging(object bindable, bool oldValue, bool newValue)
            {
                var headerControl = bindable as HeaderControl;
                headerControl.notificationPanel.IsVisible = newValue;           
            }

        private static void TextChanging(object bindable, string oldValue, string newValue)
        {
            var headerControl = bindable as HeaderControl;
            ///headerControl.iconTitleTimeTracker.IsVisible = string.IsNullOrEmpty(newValue) ? true : false;
            if (!headerControl.IsShowLogo)
            {
                headerControl.titleText.IsVisible = string.IsNullOrEmpty(newValue) ? false : true;
                headerControl.titleText.Text = newValue;
            }
        }


        private static void IsShowLogoChanging(BindableObject bindable, bool oldValue, bool newValue)
        {
            var headerControl = bindable as HeaderControl;
            if (newValue)
            {
                headerControl.imgLogo.IsVisible = newValue;
                headerControl.titleText.IsVisible = false;
            }
            else
            {
                headerControl.imgLogo.IsVisible = newValue;
                headerControl.titleText.IsVisible = true;
            }

        }


        private static void NotificationCountsChanging(object bindable, string oldValue, string newValue)
        {
            var headerControl = bindable as HeaderControl;
            ///headerControl.iconTitleTimeTracker.IsVisible = string.IsNullOrEmpty(newValue) ? true : false;

            headerControl.notificationCountPanel.IsVisible = string.IsNullOrEmpty(newValue) ? false : true;
            headerControl.notificationCounts.Text = newValue;
        }


        private static void PendingCountsChanging(object bindable, string oldValue, string newValue)
        {
            var headerControl = bindable as HeaderControl;
            ///headerControl.iconTitleTimeTracker.IsVisible = string.IsNullOrEmpty(newValue) ? true : false;

            headerControl.pendingCountPanel.IsVisible = string.IsNullOrEmpty(newValue) ? false : true;
            headerControl.pendingCount.Text = newValue;
        }

        private void ShowMenuOrGoBack(object sender, EventArgs e)
        {
            try
            {
                if (Device.Idiom == TargetIdiom.Phone || Device.Idiom == TargetIdiom.Desktop)
                {
                    (Application.Current.MainPage as MasterView).IsPresented = !(Application.Current.MainPage as MasterView).IsPresented;
                }
            }
            catch (Exception)
            {               
            }             
        }
    }
}