﻿using ClinicalIntelligence.Models;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ClinicalIntelligence.Popups
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AppointmentsPopup : PopupPage
    {

        public event EventHandler MessageEvent;
        public event EventHandler CallEvent;
        public event EventHandler CamEvent;
        public event EventHandler ChatEvent;
        public AppointmentsPopup (AppointmentsData appointmenteModel)
		{
			InitializeComponent ();
            title.Text = appointmenteModel.Title;
            doctor.Text = appointmenteModel.Doctor;
            receptionTime.Text = appointmenteModel.ReceptionTime;
            image.Source = appointmenteModel.Image;

            imgMessag.Clicked += async (se, arg) =>
            {
                if (MessageEvent != null)
                    MessageEvent.Invoke(appointmenteModel, EventArgs.Empty);
                await PopupNavigation.PopAsync();
            };

            imgTelephone.Clicked += async (se, arg) =>
            {
                if (CallEvent != null)
                    CallEvent.Invoke(appointmenteModel, EventArgs.Empty);
                await PopupNavigation.PopAsync();
            };

            imgVideo.Clicked += async (se, arg) =>
            {
                if (CamEvent != null)
                    CamEvent.Invoke(appointmenteModel, EventArgs.Empty);
                await PopupNavigation.PopAsync();
            };
            imgChat.Clicked += async (se, arg) =>
            {
                if (ChatEvent != null)
                    ChatEvent.Invoke(appointmenteModel, EventArgs.Empty);
                await PopupNavigation.PopAsync();
            };

        }

        public virtual async void CancelAsync(object sender, EventArgs args)
        {
            try
            {
                if (PopupNavigation.PopupStack.Count > 0)
                {
                    await PopupNavigation.PopAsync();
                }
            }
            catch { }
        }
    }
}