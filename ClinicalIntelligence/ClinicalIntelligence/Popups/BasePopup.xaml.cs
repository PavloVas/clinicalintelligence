﻿using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ClinicalIntelligence.Popups
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class BasePopup : PopupPage
    {
        public Button BtnOK;
        public Button BtnCancel;
        public Label LblMessage;
        public Entry InputText;
        public Editor MultiInputText;

        public BasePopup()
        {
            InitializeComponent();

            // tablet support
            this.frame.HorizontalOptions = Device.Idiom == TargetIdiom.Phone ? LayoutOptions.FillAndExpand : LayoutOptions.Center;

            BtnOK = btnOk;
            BtnCancel = btnCancel;
            LblMessage = lblMessage;
            InputText = oneLineInputText;
            MultiInputText = multiLineInputText;

            BtnOK.Text = "Ok";
            BtnCancel.Text = "Cancel";
        }

        public virtual async void ApplyAsync(object sender, EventArgs args)
        {
            try
            {
                if (PopupNavigation.PopupStack.Count > 0)
                {
                    await PopupNavigation.PopAsync();
                }
            }
            catch { }
        }

        public virtual async void CancelAsync(object sender, EventArgs args)
        {
            try
            {
                if (PopupNavigation.PopupStack.Count > 0)
                {
                    await PopupNavigation.PopAsync();
                }
            }
            catch { }
        }
    }
}
