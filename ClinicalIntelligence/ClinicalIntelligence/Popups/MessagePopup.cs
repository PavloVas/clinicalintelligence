﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClinicalIntelligence.Popups
{
    public class MessagePopup : BasePopup
    {
        public MessagePopup(string message, string okText = null)
        {
            LblMessage.Text = message;
            LblMessage.IsVisible = true;

            if (!string.IsNullOrWhiteSpace(okText))
            {
                BtnCancel.Text = okText;
            }

            BtnCancel.IsVisible = true;
        }
    }
}
