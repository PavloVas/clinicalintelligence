﻿using ClinicalIntelligence.Models;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ClinicalIntelligence.Popups
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class NotificationPopup : PopupPage
    {
        public event EventHandler RejectEvent;
        public event EventHandler AcceptEvent;

        public NotificationPopup (NotificationsModels notificationsModels )
		{
			InitializeComponent ();

            userImage.Source = notificationsModels.Image;
            name.Text = notificationsModels.Name;
            qualification.Text = notificationsModels.Qualification;


            btnRejectPopup.Clicked += async (se, arg) =>
            {
                if (RejectEvent != null)
                    RejectEvent.Invoke(notificationsModels, EventArgs.Empty);
                await PopupNavigation.PopAsync();
            };


            btnAcceptPopup.Clicked += async (se, arg) =>
            {
                if (AcceptEvent != null)
                    AcceptEvent.Invoke(notificationsModels, EventArgs.Empty);
                await PopupNavigation.PopAsync();
            };
        }

        public virtual async void CancelAsync(object sender, EventArgs args)
        {
            try
            {
                if (PopupNavigation.PopupStack.Count > 0)
                {
                    await PopupNavigation.PopAsync();
                }
            }
            catch { }
        }
    }
}