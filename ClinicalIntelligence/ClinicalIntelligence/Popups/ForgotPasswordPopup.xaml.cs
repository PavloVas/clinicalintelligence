﻿using ClinicalIntelligence.Helpers;
using ClinicalIntelligence.ViewModels;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ClinicalIntelligence.Popups
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ForgotPasswordPopup : PopupPage
    {
        protected string Email
        {
            get { return entryEmail.Text; }
            set { entryEmail.Text = value; }
        }

        protected string ErrorMessage
        {
            get { return lblErrorMessage.Text; }
            set { lblErrorMessage.Text = value; }
        }

        public ForgotPasswordPopup()
        {
            InitializeComponent();

            // tablet support
            this.frame.HorizontalOptions = Device.Idiom == TargetIdiom.Phone ? LayoutOptions.FillAndExpand : LayoutOptions.Center;
        }

        public virtual async void ApplyAsync(object sender, EventArgs args)
        {
            lblErrorMessage.IsVisible = false;
            entryEmail.HasAttention = false;

            if (string.IsNullOrWhiteSpace(Email))
            {
                entryEmail.HasAttention = true;
                lblErrorMessage.IsVisible = true;
                ErrorMessage = "Email is required";
                return;
            }

            if (EmailValidationHelper.EmailValidation(Email))
            {
                entryEmail.HasAttention = true;
                lblErrorMessage.IsVisible = true;
                ErrorMessage = "Email format is not correct.";
                return;
            }

           // var context = this.BindingContext as BaseViewModel;
            //await context.CheckConnectionAsync();
            //if (!context.HasInternetConnection)
            //{
            //    return;
            //}

           // context.ShowBusy("Verifying email address...");
            //var resultVerifyEmail = true; //await IoC.Get<ApiClient>().VerifyEmailAsync(Email);
            //context.HideBusy();

            //if (resultVerifyEmail.Success && !resultVerifyEmail.Result)
            //{
            //    context.ShowToastMessage("Email is not valid !", ToastStatus.Error);
            //    return;
            //}
            //else if (!resultVerifyEmail.Success)
            //{
            //    context.ShowToastMessage(resultVerifyEmail.MessageError, ToastStatus.Error);
            //    return;
            //}

            //context.ShowBusy();
            //var result = true; //await IoC.Get<ApiClient>().ForgotPasswordAsync(Email);
            //context.HideBusy();
            //if (result.Success && result.Result == true)
            //{
            //    try
            //    {
            //        if (PopupNavigation.PopupStack.Count > 0)
            //        {
            //            await PopupNavigation.PopAsync();
            //        }
            //    }
            //    catch { }

               // context.ShowMessagePopupAsync("A temporary password has been sent. Please check your email.");
            //    //IoC.Get<INavigationService>().For<ChangePasswordViewModel>().Navigate();
            //}
            //else
            //{
            //    context.ShowToastMessage(result.MessageError, ToastStatus.Error);
            //}
        }

        public virtual async void CancelAsync(object sender, EventArgs args)
        {
            try
            {
                if (PopupNavigation.PopupStack.Count > 0)
                {
                    await PopupNavigation.PopAsync();
                }
            }
            catch { }
        }
    }
}
