﻿using ClinicalIntelligence.Models;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ClinicalIntelligence.Popups
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class QuestionnairePopup : PopupPage
    {
        public event EventHandler FilledEvent;
        public event EventHandler ViewEvent;
        public QuestionnairePopup (QuestionnaireModel questionnaireModel)
		{
			InitializeComponent ();

            healthSurvey.Text = questionnaireModel.HealthSurvey;
            clinicName.Text = questionnaireModel.ClinicName;            


            btnFilledPopup.Clicked += async (se, arg) =>
            {
                if (FilledEvent != null)
                    FilledEvent.Invoke(questionnaireModel, EventArgs.Empty);
                await PopupNavigation.PopAsync();
            };


            btnViewPopup.Clicked += async (se, arg) =>
            {
                if (ViewEvent != null)
                    ViewEvent.Invoke(questionnaireModel, EventArgs.Empty);
                await PopupNavigation.PopAsync();
            };


        }

        public virtual async void CancelAsync(object sender, EventArgs args)
        {
            try
            {
                if (PopupNavigation.PopupStack.Count > 0)
                {
                    await PopupNavigation.PopAsync();
                }
            }
            catch { }
        }
    }
}