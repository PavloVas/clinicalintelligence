﻿using ClinicalIntelligence.API;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ClinicalIntelligence.Popups
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ToastPopup : PopupPage
    {
        private int _timeout;

        public ToastPopup(string message, ToastStatus status = ToastStatus.Default, int timeout = 1)
        {
            InitializeComponent();

            lblToastMessage.Text = message;
            _timeout = timeout;

            SetBackColor(status);
            AutoHideToast();
        }

        private async void AutoHideToast()
        {
            await Task.Delay(_timeout * 1500);


            try
            {
                if (PopupNavigation.PopupStack.Count > 0)
                {
                    await PopupNavigation.PopAsync();
                }
            }
            catch { }
        }

        private void SetBackColor(ToastStatus status)
        {
            switch (status)
            {
                case ToastStatus.Default:
                    toastBack.BackgroundColor = Theme.COLOR_PAGE_HEADER;
                    break;
                case ToastStatus.Success:
                    toastBack.BackgroundColor = Theme.COLOR_GREEN_BASE;
                    break;
                case ToastStatus.Error:
                    toastBack.BackgroundColor = Theme.COLOR_RED_BASE;
                    break;
                case ToastStatus.DeletedEntry:
                    toastBack.BackgroundColor = Theme.COLOR_GREEN_BASE;
                    break;
                case ToastStatus.SubmittedEntry:
                    toastBack.BackgroundColor = Theme.COLOR_STATUS_SUBMITTED;
                    break;
                case ToastStatus.RevertedEntry:
                    toastBack.BackgroundColor = Theme.COLOR_STATUS_REVERTED;
                    break;
                case ToastStatus.RejectedEntry:
                    toastBack.BackgroundColor = Theme.COLOR_STATUS_REJECTED;
                    break;
                case ToastStatus.ApprovedEntry:
                    toastBack.BackgroundColor = Theme.COLOR_STATUS_APPROVED;
                    break;
                default:
                    break;
            }
        }
    }
}