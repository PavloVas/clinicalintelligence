﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace ClinicalIntelligence
{
    public class Theme
    {
        public static readonly Color COLOR_ORANGE_BASE = Color.FromHex("#F58026");
        public static readonly Color COLOR_BLUE_BASE = Color.FromHex("#337ab7");
        public static readonly Color COLOR_BLACK_BASE = Color.FromHex("#000000");
        public static readonly Color COLOR_GRAY_BASE = Color.FromHex("#74736F");
        public static readonly Color COLOR_BROWN_BASE = Color.FromHex("#4c4843");

        public static readonly Color COLOR_GREEN_BASE = Color.FromHex("#50A931");
        public static readonly Color COLOR_RED_BASE = Color.FromHex("#F44D20");
        public static readonly Color COLOR_YELLOW_BASE = Color.FromHex("#FFCD31");

        public static readonly Color COLOR_BUTTON_GREEN_BACKGROUND = COLOR_GREEN_BASE;
        public static readonly Color COLOR_BUTTON_GREEN = COLOR_GREEN_BASE;


        public static readonly Color COLOR_PAGE_BACKGROUND = Color.FromHex("#FFF6F6F6");

        public static readonly Color COLOR_PAGE_HEADER = COLOR_BROWN_BASE;


        public static readonly Color COLOR_ERROR_BACKGROUND_RED = Color.FromHex("#FFF2E9E6");
        public static readonly Color COLOR_ERROR_TEXT_RED = Color.FromHex("#FFF3724F");


        public static readonly Color COLOR_TEXT_GRAY = Color.FromHex("#9b9b9a");
        public static readonly Color COLOR_TEXT_BLUE = Color.FromHex("#4793D9");

        public static readonly Color COLOR_BUTTON_ORANGE = COLOR_ORANGE_BASE;
        public static readonly Color COLOR_BUTTON_GRAY = COLOR_GRAY_BASE;

        /* -- STATUSES STYLES -- */
        public static readonly Color COLOR_STATUS_SUBMITTED = COLOR_BLUE_BASE;
        public static readonly Color COLOR_STATUS_REVERTED = COLOR_YELLOW_BASE;
        public static readonly Color COLOR_STATUS_REJECTED = COLOR_RED_BASE;
        public static readonly Color COLOR_STATUS_APPROVED = COLOR_GREEN_BASE;
    }
}
