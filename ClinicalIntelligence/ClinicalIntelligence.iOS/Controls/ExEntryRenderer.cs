﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using ClinicalIntelligence.Controls;
using ClinicalIntelligence.iOS.Controls;
using CoreGraphics;
using Foundation;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms.PlatformConfiguration;

[assembly: ExportRenderer(typeof(ExEntry), typeof(ExEntryRenderer))]

namespace ClinicalIntelligence.iOS.Controls
{
    public class ExEntryRenderer : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            var view = (ExEntry)Element;

            //if (Control != null)
            //{
            //  // draw gray bord
            //  Control.BorderStyle = UITextBorderStyle.None;
            //  Control.Layer.CornerRadius = 0;
            //  Control.Layer.MasksToBounds = true;
            //  Control.Layer.BorderColor = Theme.COLOR_BORDER_TEXT_GRAY.ToCGColor();
            //  Control.Layer.BorderWidth = 1;
            //}

            var toolbar = new UIToolbar(new CGRect(0.0f, 0.0f, Control.Frame.Size.Width, 44.0f));
            toolbar.Items = new[]
            {
                new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace),
              //  new UIBarButtonItem(TranslateExtension.GetTranslatedValue("Common_Done"), UIBarButtonItemStyle.Done, delegate { Control.ResignFirstResponder(); })
            };

            Control.InputAccessoryView = toolbar;

            UpdateStyles(view);
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            var view = (ExEntry)Element;

            UpdateStyles(view);
        }

        private void UpdateStyles(ExEntry view)
        {
            if (Control == null || view == null)
            {
                return;
            }

            if (view.IsNumeric)
            {
                Control.KeyboardType = UIKeyboardType.NumbersAndPunctuation;
            }
        }
    }
}
