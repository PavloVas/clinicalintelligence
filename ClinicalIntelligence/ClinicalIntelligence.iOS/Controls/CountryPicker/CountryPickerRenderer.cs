﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using XamCountryPicker;
using XamCountryPicker.iOS;

[assembly: ExportRenderer(typeof(CountryPicker), typeof(CountryPickerRenderer))]
namespace XamCountryPicker.iOS
{
    public class CountryPickerRenderer : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement == null)
            {
                // Disable user input
                Control.ShouldChangeCharacters = (a, b, c) => false;
            }
        }

    }
}