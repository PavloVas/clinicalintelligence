﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClinicalIntelligence.Controls;
using ClinicalIntelligence.iOS.Controls;
using Foundation;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(BoxViewCircleControl), typeof(BoxViewCircleRenderer))]
namespace ClinicalIntelligence.iOS.Controls
{
   public  class BoxViewCircleRenderer : BoxRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<BoxView> e)
        {
            base.OnElementChanged(e);

            if (Element == null)
                return;

            Layer.MasksToBounds = true;
            Layer.CornerRadius = (float)((BoxViewCircleControl)this.Element).CornerRadius / 2.0f;
        }
    }
}