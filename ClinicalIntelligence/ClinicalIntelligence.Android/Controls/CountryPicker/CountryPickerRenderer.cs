﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Text;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using XamCountryPicker;
using XamCountryPicker.Droid;

[assembly: ExportRenderer(typeof(CountryPicker), typeof(CountryPickerRenderer))]
namespace XamCountryPicker.Droid
{
    public class CountryPickerRenderer : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                // Diasble user input
                Control.InputType = InputTypes.Null;
            }
        }
    }
}