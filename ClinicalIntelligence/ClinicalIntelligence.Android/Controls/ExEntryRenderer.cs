﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Text;
using Android.Views;
using Android.Widget;
using ClinicalIntelligence.Controls;
using ClinicalIntelligence.Droid.Controls;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(ExEntry), typeof(ExEntryRenderer))]

namespace ClinicalIntelligence.Droid.Controls
{
    public class ExEntryRenderer : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            var view = (ExEntry)Element;

            Control.ImeOptions = Android.Views.InputMethods.ImeAction.Done;
            UpdateStyles(view);
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            var view = (ExEntry)Element;

            UpdateStyles(view);
        }

        private void UpdateStyles(ExEntry view)
        {
            if (Control == null)
            {
                return;
            }

            if (view.IsNumeric)
            {
                Control.InputType = InputTypes.ClassText;
            }
        }
    }
}