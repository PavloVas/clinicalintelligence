﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ClinicalIntelligence.Controls;
using ClinicalIntelligence.Droid.Controls;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(BorderlessButton), typeof(BorderlessButtonRenderer))]

namespace ClinicalIntelligence.Droid.Controls
{
    public class BorderlessButtonRenderer : Xamarin.Forms.Platform.Android.AppCompat.ButtonRenderer
    {
        private GradientDrawable _normal, _pressed;

        public BorderlessButtonRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Button> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                var button = e.NewElement;

                // Create a drawable for the button's normal state
                _normal = new GradientDrawable();

                _normal.SetStroke((int)button.BorderWidth, button.BorderColor.ToAndroid());
                _normal.SetCornerRadius(0);

                // Create a drawable for the button's pressed state
                _pressed = new GradientDrawable();
                // var highlight = Context.ObtainStyledAttributes(new int[] { Android.Resource.Attribute.ColorActivatedHighlight }).GetColor(0, Android.Graphics.Color.Gray);
                _pressed.SetColor(Android.Graphics.Color.LightGray);
                _pressed.SetStroke((int)button.BorderWidth, button.BorderColor.ToAndroid());
                _pressed.SetCornerRadius(button.BorderRadius);

                //// Add the drawables to a state list and assign the state list to the button
                //var sld = new StateListDrawable();
                //sld.AddState(new int[] { Android.Resource.Attribute.StatePressed }, _pressed);
                //sld.AddState(new int[] { }, _normal);
                //Control.SetBackground(sld);
                //Control.SetPadding(0, 1, 0, 0);
            }
        }
    }
}